#!/bin/bash

# Ask for the user password
# Script only works if sudo caches the password for a few minutes
sudo true
	
# Prevent sudo timeout
sudo -v # ask for sudo password up-front
while true; do
  # Update user's timestamp without running a command
  sudo -nv; sleep 1m
  # Exit when the parent process is not running any more. In fact this loop
  # would be killed anyway after being an orphan(when the parent process
  # exits). But this ensures that and probably exit sooner.
  kill -0 $$ 2>/dev/null || exit
done &	

if [ -e "/etc/init.d/glassfish" ]; then
	echo "Parando/desativando servico local : glassfish"
	sudo service glassfish stop;
	sudo update-rc.d glassfish disable;
fi

if [ -e "/etc/init.d/postgresql" ]; then
	echo "Parando/desativando servico local : postgresql"
	sudo service postgresql stop;
	sudo update-rc.d postgresql disable;
fi

if [ -e "/etc/init.d/mongodb" ]; then
	echo "Parando/desativando servico local : mongodb"
	sudo service mongodb stop;
	sudo update-rc.d mongodb disable;
fi

# Add Docker PPA and install latest version
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
# sudo sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
# sudo apt-get update
# sudo apt-get install lxc-docker -y

# removing old docker 
sudo apt remove docker* -y  
sudo apt-get autoremove -y
sudo apt-get autoclean -y

# install 
sudo apt-get install git curl wget apt-utils openssh-server software-properties-common python-software-properties -y --allow-unauthenticated

# Alternatively you can use the official docker install script
sudo wget --progress=bar:force:noscroll -qO- https://get.docker.com/ | sudo sh

# REPOSITORIOS PG 96
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"
sudo wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

# Install docker-compose
#COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9][0-9]+\.[0-9]+$" | tail -n 1`
COMPOSE_VERSION=1.18.0
sudo sh -c "sudo curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose
sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose"

# docker prune
sudo docker system prune --all --force

# Install ctop
sudo wget --progress=bar:force:noscroll https://github.com/bcicen/ctop/releases/download/v0.5/ctop-0.5-linux-amd64 -O /usr/local/bin/ctop
sudo chmod +x /usr/local/bin/ctop		
	
# git clone sgv-docker repo
sudo rm -Rf /java
sudo mkdir -p /java
sudo git clone https://lucasdallagnol10@bitbucket.org/aloingressos/sgv-docker.git /java/sgv-docker
cd /java/sgv-docker
sudo git fetch --all && sudo git reset --hard HEAD && sudo git clean -f -d && sudo git pull
sudo chmod 775 /java/sgv-docker

sudo docker-compose down
sudo docker volume remove sgvdocker_db-data
sudo docker-cleanup
sudo docker volume prune -f
sudo docker container prune -f
sudo docker container rm tomcat -f
sudo docker container rm postgres -f
sudo docker container rm mongodb -f

# remove webapps antigas
sudo rm -Rf /java/tomcat/webapps
			
sudo docker-compose build --no-cache
	
sudo docker-compose up -d

sudo chmod -Rf 775 *
sudo ./updateMongo.sh
sudo ./updateSgv.sh
sudo ./updateStatic.sh
sudo ./updateValidator.sh
sudo ./resetDb.sh